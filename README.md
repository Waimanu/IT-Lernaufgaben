# IT Lernaufgaben

## Index

1. Index
2. Aufgaben

## Aufgaben

### Arbeiten mit git

Git ist heute nicht mehr aus der IT wegzudenken und daher auch das erste mit dem wir uns beschäftigen.

#### Aufgabe

1. Clone dieses Repo
2. Erstelle einen Branch
3. Füge dich in die folgende "Bestanden" Liste ein.
4. Stelle einen Mergerequest

## Hilfe

* [Git Grundlagen: Ein Git Repository anlegen](https://git-scm.com/book/de/v2/Git-Grundlagen-Ein-Git-Repository-anlegen)

##### Bestenden

* …
